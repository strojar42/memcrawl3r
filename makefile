SHELL := /bin/bash

CC=gcc-7

architecture=64

ifeq ($(architecture),32)
	ld_architecture=-m elf_i386
	sharedlib_stack_protector=-fno-stack-protector
endif

stack_limit=$(shell echo "$$(($$(ulimit -s)*1024))")

default_flags=-m$(architecture) -std=c99
basic_flags=-O2 -Wall -Wextra -pedantic -Wl,--build-id=sha1
warn_flags=-Waggregate-return -Walloc-zero -Walloca -Warray-bounds=2 -Wbad-function-cast -Wcast-align -Wcast-qual -Wconversion -Wdate-time -Wdisabled-optimization -Wdouble-promotion -Wduplicated-branches -Wduplicated-cond -Wfloat-equal -Wformat=2 -Wformat-overflow=2 -Wformat-truncation=2 -Wformat-signedness -Winit-self -Winline -Winvalid-pch -Wjump-misses-init -Wlogical-op -Wmissing-declarations -Wmissing-include-dirs -Wmissing-prototypes -Wmultichar -Wnested-externs -Wnull-dereference -Wold-style-definition -Wpacked -Wpadded -Wredundant-decls -Wrestrict -Wshadow -Wno-shadow-compatible-local -Wshift-overflow=2 -Wstack-usage=$(stack_limit) -Wstrict-overflow=5 -Wstrict-prototypes -Wtrampolines -Wundef -Wunsafe-loop-optimizations -Wunused-const-variable=2 -Wunused-macros -Wwrite-strings
hardened_flags=-fPIE -pie -fsanitize=address -fsanitize=undefined -Wno-padded -fno-sanitize-recover=all -fstack-check -Wstack-protector -fstack-protector-all -fstack-protector-strong -Wl,-z,relro -Wl,-z,now -fvisibility=hidden -Wl,--discard-all -Wl,--no-undefined

CFLAGS=$(default_flags) $(basic_flags) $(warn_flags)
CFLAGS_MEMCRAWL3R_LIBRARY=$(default_flags) $(basic_flags) $(warn_flags) $(hardened_flags) $(sharedlib_stack_protector) -fPIC -fpic -fvisibility=default
CFLAGS=$(default_flags) $(basic_flags) $(warn_flags) $(hardened_flags)

target=main
sources=$(target).c
args=

.PHONY: sharedlib sharedlib-runnable sharedlib-empty compile compile-static compile-shared debug preprocessor assembly assembly-debug disasm disasm-code ndisasm run run-static preload preload-runnable runtime runv runvv runall clean
.SUFFIXES:
.DEFAULT_GOAL:=compile

libmemcrawl3r.so sharedlib: memcrawl3r.c
	@ echo "*** GCC - libmemcrawl3r.so ***"
	@ $(CC) -c -fPIC -nostdlib $(CFLAGS_MEMCRAWL3R_LIBRARY) -DMEMCRAWL3R_SHARED memcrawl3r.c  -o memcrawl3r.o
	@ ld $(ld_architecture) -init init -shared memcrawl3r.o -ldl -o libmemcrawl3r.so
	@ rm -f memcrawl3r.o

libmemcrawl3r_runnable.so sharedlib-runnable: memcrawl3r.c
	@ echo "*** GCC - libmemcrawl3r.so - runnable ***"
	@ $(CC) -shared -fPIC -pie -Wl,-E $(CFLAGS_MEMCRAWL3R_LIBRARY) -Wl,-init,init -DMEMCRAWL3R_SHARED memcrawl3r.c -ldl -o libmemcrawl3r_runnable.so

libmemcrawl3r_empty.so sharedlib-empty: memcrawl3r_empty.c
	@ echo "*** GCC - libmemcrawl3r_empty.so ***"
	@ $(CC) -c -fPIC -nostdlib $(CFLAGS_MEMCRAWL3R_LIBRARY) memcrawl3r_empty.c -o memcrawl3r_empty.o
	@ ld $(ld_architecture) -shared memcrawl3r_empty.o -o libmemcrawl3r_empty.so
	@ rm -f memcrawl3r_empty.o

$(target) compile: $(sources)
	@ echo "*** GCC - main ***"
	@ $(CC) $(CFLAGS) $(sources) memcrawl3r_empty.c -o $(target)

$(target)-static compile-static: $(sources) memcrawl3r.c
	@ echo "*** GCC - main-static ***"
	@ $(CC) $(CFLAGS) $(sources) memcrawl3r.c -ldl -o $(target)

$(target)-shared compile-shared: $(sources) sharedlib-empty
	@ echo "*** GCC - main-shared ***"
	@ $(CC) $(CFLAGS) -Wl,-rpath=. $(sources) libmemcrawl3r_empty.so -o $(target)

$(target)_g debug: $(sources)
	@ echo "*** GCC ***"
	@ $(CC) -g $(CFLAGS) $(sources) memcrawl3r_empty.c -o $(target)_g

preprocessor: $(sources)
	@ echo "*** GCC ***"
	@ $(CC) -E $(CFLAGS) $(sources) -o $(target).i

assembly: $(sources)
	@ echo "*** GCC ***"
	@ $(CC) -S $(CFLAGS) $(sources) -o $(target).s

assembly-debug: $(sources)
	@ echo "*** GCC ***"
	@ $(CC) -g -S $(CFLAGS) $(sources) -o $(target)_g.s

disasm: $(target)
	objdump -s -x -g -d $(target) > $(target).disasm

disasm-code: $(target)_g
	objdump -s -x -g -S -d $(target)_g > $(target)_g.disasm

ndisasm: $(target)
	objcopy -j .text -S -O binary $(target) $(target).bin
	ndisasm -b$(architecture) $(target).bin > $(target).ndisasm

run: compile
	@ echo "*** RUN ***"
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ ./$(target) $(args)
else
	@ ulimit -v unlimited;\
	./$(target) $(args)
endif

run-static: compile-static
	@ echo "*** RUN - STATIC ***"
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ ./$(target) $(args)
else
	@ ulimit -v unlimited;\
	./$(target) $(args)
endif

preload: compile-shared sharedlib
	@ echo "*** RUN - PRELOAD ***"
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ LD_PRELOAD="libmemcrawl3r.so" ./$(target) $(args)
else
	@ ulimit -v unlimited;\
	LD_PRELOAD="libasan.so.4 libmemcrawl3r.so" ./$(target) $(args)
endif

preload-runnable: compile-shared sharedlib-runnable
	@ echo "*** RUN - libmemcrawl3r_runnable.so ***"
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS_MEMCRAWL3R_LIBRARY)),)
	@ ./libmemcrawl3r_runnable.so
else
	@ ulimit -v unlimited;\
	./libmemcrawl3r_runnable.so
endif
	@ echo "*** RUN - PRELOAD ***"
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ LD_PRELOAD="libmemcrawl3r_runnable.so" ./$(target) $(args)
else
	@ ulimit -v unlimited;\
	LD_PRELOAD="libasan.so.4 libmemcrawl3r_runnable.so" ./$(target) $(args)
endif

runtime: $(target)
	time ./$(target) $(args)

runv: $(target)_g
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ echo "*** VALGRIND ***"
	@ valgrind ./$(target)_g $(args)
else
	@ echo "*** ASan ***"
	@ ulimit -v unlimited;\
	./$(target)_g $(args)
endif

runvv: $(target)_g
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ echo "*** VALGRIND ***"
	@ time valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes --malloc-fill=0x55 ./$(target)_g $(args)
else
	@ echo "*** ASan ***"
	@ ulimit -v unlimited;\
	time ./$(target)_g $(args)
endif

runall: $(target)_g
ifeq ($(findstring $-$-fsanitize=,$(CFLAGS)),)
	@ echo "*** VALGRIND ***"
	@ /usr/bin/time -v valgrind -v --leak-check=full --show-leak-kinds=all --track-origins=yes --malloc-fill=0x55 ./$(target)_g $(args)
else
	@ echo "*** ASan ***"
	@ ulimit -v unlimited;\
	/usr/bin/time -v ./$(target)_g $(args)
endif

clean:
	@ echo "*** CLEAN ***"
	rm -f $(target) $(target)_g $(target).i $(target).disasm $(target)_g.disasm $(target).s $(target)_g.s $(target).bin $(target).ndisasm memcrawl3r.o memcrawl3r_empty.o libmemcrawl3r.so libmemcrawl3r_empty.so libmemcrawl3r_runnable.so
