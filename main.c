#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "memcrawl3r.h"

#define debug(fmt,...) fprintf(stderr,"main: \033[1;34mDEBUG:\033[0m "fmt"\n",__VA_ARGS__);

static void *alloc_2d_array(size_t x,size_t y,size_t size)
{
	void **arr=NULL;
	arr=(void*)malloc(x*sizeof(void*)+x*y*size);
	if(arr==NULL)
	{
		return NULL;
	}
	arr[0]=(char*)arr+x*sizeof(void*);
	for(size_t i=1;i<x;++i)
	{
		arr[i]=(char*)arr[0]+i*y*size;
	}
	return arr;
}

static void free_2d_array(void *arr)
{
	if(arr!=NULL)
	{
		free(arr);
	}
	return;
}

int main(int argc __attribute__((unused)), char *argv[] __attribute__((unused)), char *envp[] __attribute__((unused)))
{
	int **arr=NULL;
	size_t x=3;
	size_t y=5;
	size_t soa=x*sizeof(*arr)+x*y*sizeof(**arr);
	size_t swb=soa+8;
	size_t srt=(((soa+8)/16)+1)*16;

	arr=alloc_2d_array(x,y,sizeof(**arr));	// 84 bytes allocated
	debug("size of allocation %zu",soa)
	debug("size with bookkeeping %zu",swb)
	debug("size rounded to blocks %zu",srt)
	debug("arr at 0x%"PRIxPTR,(uintptr_t)arr)

	memcrawl3r_highlight mcr_hl[1]={{arr,soa}};
	debug("memcrawl3r returned %zu",memcrawl3r(arr,16,112,1,mcr_hl,stdout))
	free_2d_array(arr);
	return 0;
}
