#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <inttypes.h>
#include <dlfcn.h>
#include "memcrawl3r.h"

#define MEMCRAWL3R_NAME "memcrawl3r"
#define MEMCRAWL3R_USE_COLORS
//#define MEMCRAWL3R_DEBUG
//#define MEMCRAWL3R_SHARED
#ifndef MEMCRAWL3R_SIGNAL_OVERRIDE
	#define MEMCRAWL3R_SIGNAL_OVERRIDE 2
#endif

#ifdef MEMCRAWL3R_USE_COLORS
	#define MEMCRAWL3R_PRINT_RED "\033[1;31m"
	#define MEMCRAWL3R_PRINT_BLUE "\033[1;34m"
	#define MEMCRAWL3R_PRINT_GREEN "\033[1;32m"
	#define MEMCRAWL3R_PRINT_MAGENTA "\033[1;35m"
	#define MEMCRAWL3R_PRINT_INVERT "\033[7m"
	#define MEMCRAWL3R_PRINT_RESET "\033[0m"
#else
	#define MEMCRAWL3R_PRINT_RED ""
	#define MEMCRAWL3R_PRINT_BLUE ""
	#define MEMCRAWL3R_PRINT_GREEN ""
	#define MEMCRAWL3R_PRINT_MAGENTA ""
	#define MEMCRAWL3R_PRINT_INVERT ""
	#define MEMCRAWL3R_PRINT_RESET ""
#endif



#define mcr_error(fmt,...) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_RED "ERROR:" MEMCRAWL3R_PRINT_RESET " "fmt"\n",__VA_ARGS__);
#define mcr_func_error(func,fmt,...) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_GREEN "%s:" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_RED "ERROR:" MEMCRAWL3R_PRINT_RESET " "fmt"\n",func,__VA_ARGS__);

#ifdef MEMCRAWL3R_DEBUG
	#define mcr_debug(txt) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_BLUE "DEBUG:" MEMCRAWL3R_PRINT_RESET " "txt"\n");
	#define mcr_debug_p(fmt,...) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_BLUE "DEBUG:" MEMCRAWL3R_PRINT_RESET " "fmt"\n",__VA_ARGS__);
	#define mcr_func_debug(func,txt) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_GREEN "%s:" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_BLUE "DEBUG:" MEMCRAWL3R_PRINT_RESET " "txt"\n",func);
	#define mcr_func_debug_p(func,fmt,...) fprintf(stderr,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_GREEN "%s:" MEMCRAWL3R_PRINT_RESET " " MEMCRAWL3R_PRINT_BLUE "DEBUG:" MEMCRAWL3R_PRINT_RESET " "fmt"\n",func,__VA_ARGS__);
#else
	#define mcr_debug(txt)
	#define mcr_debug_p(fmt,...)
	#define mcr_func_debug(func,txt)
	#define mcr_func_debug_p(func,fmt,...)
#endif

#ifdef __x86_64__
	#define MEMCRAWL3R_REG_IP REG_RIP
	#define MEMCRAWL3R_RIP_RELATIVE "[rip]"
#else
	#define MEMCRAWL3R_REG_IP REG_EIP
	#define MEMCRAWL3R_RIP_RELATIVE
#endif

unsigned long (*memcrawl3r_empty)(void *addr, size_t max_bytes_before, size_t max_bytes_after, int hl_len, memcrawl3r_highlight *mcr_hl)=NULL;

#ifdef MEMCRAWL3R_SHARED
	void init(void);

	void init(void)
	{
		*(void **)(&memcrawl3r_empty)=dlsym(RTLD_NEXT,"memcrawl3r");
	}

	int main(int argc, char *argv[], char *envp[])
	{
		size_t len=0;
		memcrawl3r_highlight mcr_hl[3]={{&argc,sizeof(argc)},{&argv,sizeof(&argv)},{&envp,sizeof(&envp)}};

		printf("arguments 'argc, argv, envp' on main()'s stack:\n");
		memcrawl3r(&argc,32,32,3,mcr_hl,stdout);

		mcr_hl[0].addr=argv;
		mcr_hl[0].len=sizeof(char*);
		printf("memory pointed by 'argv', contains pointer to 'argv[0]':\n");
		memcrawl3r(argv,32,32,1,mcr_hl,stdout);

		for(len=0;argv[0][len]!=0;++len);	// simple strlen(), i didn't want to include whole <string.h>
		mcr_hl[0].addr=argv[0];
		mcr_hl[0].len=len+1;
		printf("memory pointed by 'argv[0]', contains value of first argument - this program's name '%s':\n",argv[0]);
		memcrawl3r(argv[0],32,32,1,mcr_hl,stdout);
	}
#endif

#if MEMCRAWL3R_SIGNAL_OVERRIDE == 1
	const char *__asan_default_options()
	{
		return "allow_user_segv_handler=1";
	}
#endif

static sig_atomic_t mcr_stage=0;

static void memcrawl3r_sigsegv_handler(int __attribute__((unused)) signal, siginfo_t __attribute__((unused)) *si, void *context)
{
	void *addr_ip=0;
	void *recover_ip=0;
	mcr_func_debug_p("signal handler","got signal %d at stage %d",signal,mcr_stage)
	mcr_func_debug_p("signal handler","SIGSEGV at %p",si->si_addr)
	mcr_func_debug_p("signal handler","RIP is %p",(void*)((ucontext_t*)context)->uc_mcontext.gregs[MEMCRAWL3R_REG_IP])
	if(mcr_stage==1)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n\t"
			"lea %[ip1],memcrawl3r_address1"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			"lea %[ip2],memcrawl3r_recover1"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			".att_syntax"
			://output
			[ip1]"=r"(addr_ip),
			[ip2]"=r"(recover_ip)
			://input
			://clobber
		);
	}
	else if(mcr_stage==2)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n\t"
			"lea %[ip1],memcrawl3r_address2"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			"lea %[ip2],memcrawl3r_recover2"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			".att_syntax"
			://output
			[ip1]"=r"(addr_ip),
			[ip2]"=r"(recover_ip)
			://input
			://clobber
		);
	}
	else if(mcr_stage==3)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n\t"
			"lea %[ip1],memcrawl3r_address3"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			"lea %[ip2],memcrawl3r_recover3"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			".att_syntax"
			://output
			[ip1]"=r"(addr_ip),
			[ip2]"=r"(recover_ip)
			://input
			://clobber
		);
	}
	else if(mcr_stage==4)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n\t"
			"lea %[ip1],memcrawl3r_address4"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			"lea %[ip2],memcrawl3r_recover4"MEMCRAWL3R_RIP_RELATIVE"\n\t"
			".att_syntax"
			://output
			[ip1]"=r"(addr_ip),
			[ip2]"=r"(recover_ip)
			://input
			://clobber
		);
	}
	else if(mcr_stage==5)
	{
		mcr_func_error("signal handler","SIGSEGV happened at stage %d, cannot recover, aborting",mcr_stage)
		exit(42);
	}
	else
	{
		mcr_func_error("signal handler","SIGSEGV happened at unknown stage: %d, aborting",mcr_stage)
		exit(42);
	}
	mcr_func_debug_p("signal handler","address of stage %d instruction is %p",mcr_stage,addr_ip);
	mcr_func_debug_p("signal handler","recovery point is at %p, instruction has %"PRIdPTR" bytes",recover_ip,(intptr_t)recover_ip-(intptr_t)addr_ip);
	mcr_stage=0;
	if(addr_ip==(void*)((ucontext_t*)context)->uc_mcontext.gregs[MEMCRAWL3R_REG_IP])
	{
		((ucontext_t*)context)->uc_mcontext.gregs[MEMCRAWL3R_REG_IP]=(intptr_t)recover_ip;
		mcr_func_debug("signal handler","RIP contains address of known stage, recovering after instruction")
	}
	else
	{
		mcr_func_error("signal handler","SIGSEGV happened at unknown RIP: %p, aborting",(void*)((ucontext_t*)context)->uc_mcontext.gregs[MEMCRAWL3R_REG_IP])
		exit(42);
	}
}

size_t memcrawl3r(void *addr, size_t max_bytes_before, size_t max_bytes_after, int hl_len, memcrawl3r_highlight *mcr_hl, FILE *outfile)
{
	unsigned char val;
	uintptr_t offset=0;
	size_t printed=0;
	size_t bytes=0;
	int i=0;
	int colored;
	if(mcr_stage!=0)
	{
		mcr_error("unknown starting stage: %d, exiting",mcr_stage)
		return 0;
	}

	int (*real_sigaction)(int signum, const struct sigaction *act, struct sigaction *oldact)=sigaction;
	void *libc_pointer=NULL;

	if(MEMCRAWL3R_SIGNAL_OVERRIDE == 2)
	{
		// get sigaction() directly from libc.so.6
		const char *dlsym_error;
		libc_pointer=dlopen("libc.so.6",RTLD_NOW);
		if(libc_pointer==NULL)
		{
			mcr_func_error("dlopen","%s",dlerror())
			return 0;
		}
		dlerror();
		*(void **)(&real_sigaction)=dlsym(libc_pointer,"sigaction");
		if((dlsym_error=dlerror()))
		{
			mcr_func_error("dlsym","%s",dlsym_error)
			return 0;
		}
		mcr_debug("got sigaction() directly from libc.so.6")
	}

	// create signal handler
	struct sigaction sa_new={0};
	struct sigaction sa_old={0};
	sigemptyset(&sa_new.sa_mask);
	sa_new.sa_sigaction=memcrawl3r_sigsegv_handler;
	sa_new.sa_flags=SA_SIGINFO;
	real_sigaction(SIGSEGV, &sa_new, &sa_old);
	mcr_debug("created custom SIGSEGV handler")

	// stage 1 - check if addr is readable at all
	mcr_stage=1;
	mcr_debug_p("starting at address %p - stage %d",addr,mcr_stage)
	__asm__ __volatile__
	(
		".intel_syntax noprefix\n"
		"memcrawl3r_address1:\n\t"
		"mov %[val],[%[mem]]\n"
		"memcrawl3r_recover1:\n\t"
		".att_syntax"
		://output
		[val]"=r"(val)
		://input
		[mem]"r"(addr)
		://clobber
		"memory"
	);
	if(mcr_stage!=1)
	{
		mcr_error("address %p is not directly readable, exiting",addr)
		return 0;
	}

	// stage 2 - find address of first readable byte
	mcr_stage=2;
	mcr_debug_p("address %p is directly readable, entering stage %d",addr,mcr_stage)
	while(mcr_stage==2)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n"
			"memcrawl3r_address2:\n\t"
			"mov %[val],[%[mem]+%[offs]]\n"
			"memcrawl3r_recover2:\n\t"
			".att_syntax"
			://output
			[val]"=q"(val)
			://input
			[mem]"r"(addr),
			[offs]"r"(offset)
			://clobber
			"memory"
		);
		--offset;
	}
	offset=offset+2;
	mcr_debug_p("%"PRIuPTR" bytes readable before given address",-offset)
	if(outfile==stdout||outfile==stderr)
	{
		fprintf(outfile,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " %"PRIuPTR" bytes readable before given address\n",-offset);
	}
	else
	{
		fprintf(outfile,MEMCRAWL3R_NAME ": %"PRIuPTR" bytes readable before given address\n",-offset);
	}
	if(offset<-max_bytes_before)
	{
		offset=-max_bytes_before;
	}
	if(max_bytes_after!=0)
	{
		max_bytes_after=max_bytes_after-offset;
	}

	// stage 3 - read and print readable bytes
	mcr_stage=3;
	mcr_debug_p("entering stage %d at address %p",mcr_stage,(char*)addr+offset)
	fprintf(outfile,"%p ",(void*)((((uintptr_t)addr+offset)/16)*16));
	while(((uintptr_t)addr+offset-printed)%16!=0)
	{
		fprintf(outfile,"   ");
		++printed;
		if(printed%8==0 && ((uintptr_t)addr+offset-printed)%16!=0)
		{
			fprintf(outfile," ");
		}
	}
	while(mcr_stage==3)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n"
			"memcrawl3r_address3:\n\t"
			"mov %[val],[%[mem]+%[offs]]\n"
			"memcrawl3r_recover3:\n\t"
			".att_syntax"
			://output
			[val]"=q"(val)
			://input
			[mem]"r"(addr),
			[offs]"r"(offset)
			://clobber
			"memory"
		);
		if(mcr_stage==3)
		{
			if(printed)
			{
				if(printed%16==0)
				{
					fprintf(outfile,"\n%p ",(char*)addr+offset);
				}
				else if(printed%8==0)
				{
					fprintf(outfile," ");
				}
			}
			if(outfile==stdout||outfile==stderr)
			{
				colored=0;
				for(i=0;i<hl_len;++i)
				{
					if((char*)mcr_hl[i].addr<=(char*)addr+offset&&(char*)mcr_hl[i].addr+mcr_hl[i].len>(char*)addr+offset)
					{
						//++colored;
						if(colored)
						{
							colored=-1;
							break;
						}
						colored=i%2+1;
					}
				}
				if(colored==0)
				{
					if(offset!=0)
					{
						fprintf(outfile," %.2x",val);
					}
					else
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_INVERT "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
				}
				else if(colored==1)
				{
					if(offset!=0)
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_RED "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
					else
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_INVERT MEMCRAWL3R_PRINT_RED "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
				}
				else if(colored==2)
				{
					if(offset!=0)
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_BLUE "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
					else
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_INVERT MEMCRAWL3R_PRINT_BLUE "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
				}
				else// if(colored==-1)
				{
					if(offset!=0)
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_MAGENTA "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
					else
					{
						fprintf(outfile," " MEMCRAWL3R_PRINT_INVERT MEMCRAWL3R_PRINT_MAGENTA "%.2x" MEMCRAWL3R_PRINT_RESET,val);
					}
				}
			}
			else
			{
				fprintf(outfile," %.2x",val);
			}
			++printed;
			++bytes;
		}
		++offset;
		if(bytes==max_bytes_after)
		{
			break;
		}
	}
	fprintf(outfile,"\n");
	mcr_debug_p("%zu bytes printed",bytes)
	if(mcr_stage==3)
	{
		mcr_stage=4;
		mcr_debug_p("entering stage %d at address %p",mcr_stage,(char*)addr+offset)
	}
	while(mcr_stage==4)
	{
		__asm__ __volatile__
		(
			".intel_syntax noprefix\n"
			"memcrawl3r_address4:\n\t"
			"mov %[val],[%[mem]+%[offs]]\n"
			"memcrawl3r_recover4:\n\t"
			".att_syntax"
			://output
			[val]"=q"(val)
			://input
			[mem]"r"(addr),
			[offs]"r"(offset)
			://clobber
			"memory"
		);
		++offset;
	}
	offset=offset-2;
	mcr_debug_p("%"PRIuPTR" bytes readable after given address",offset);
	if(outfile==stdout||outfile==stderr)
	{
		fprintf(outfile,MEMCRAWL3R_PRINT_RED MEMCRAWL3R_NAME ":" MEMCRAWL3R_PRINT_RESET " %"PRIuPTR" bytes readable after given address\n",offset);
	}
	else
	{
		fprintf(outfile,MEMCRAWL3R_NAME ": %"PRIuPTR" bytes readable after given address\n",offset);
	}
	if(memcrawl3r_empty!=NULL)
	{
		mcr_stage=5;
		mcr_debug_p("entering stage %d",mcr_stage)
		memcrawl3r_empty(addr,max_bytes_before,max_bytes_after,hl_len,mcr_hl);
		mcr_debug("returned from original memcrawl3r function")
	}
	mcr_stage=0;
	real_sigaction(SIGSEGV, &sa_old, NULL);
	if(libc_pointer!=NULL)
	{
		dlclose(libc_pointer);
	}
	mcr_debug("restored original SIGSEGV handler, exiting")
	return bytes;
}
