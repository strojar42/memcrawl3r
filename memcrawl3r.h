#ifndef MEMCRAWL3R
#define MEMCRAWL3R

typedef struct
{
	void* addr;
	size_t len;
} memcrawl3r_highlight;

size_t memcrawl3r(void *addr, size_t max_bytes_before, size_t max_bytes_after, int hl_len, memcrawl3r_highlight *mcr_hl, FILE *outfile);

#endif /* MEMCRAWL3R */
