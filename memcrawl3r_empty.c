#include <stdio.h>
#include "memcrawl3r.h"

size_t memcrawl3r(void __attribute__((unused)) *addr, size_t __attribute__((unused)) max_bytes_before, size_t __attribute__((unused)) max_bytes_after, int __attribute__((unused)) hl_len, memcrawl3r_highlight __attribute__((unused)) *mcr_hl, FILE __attribute__((unused)) *outfile)
{
	return 0;
}
