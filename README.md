# MEMCRAWL3R
Tool for printing the content of a whole memory segment assigned to a process, overriding common protecting techniques.

The main idea was to create some kind of a low-level memory debug tool that can print contents of the memory in an easily readable format, which does not terminate on a segmentation fault when it reaches out of memory bounds. Primarily used as a proof-of-concept of how malloc()'s bookkeeping works in linux, and how it is changing with tools like [ASan](https://github.com/google/sanitizers/wiki/AddressSanitizer) or [Valgrind](http://www.valgrind.org/).

## Usage
Memcrawl3r is designed to run in three different modes:
 - compiled directly as a C module within program  
   Just include the [header file](memcrawl3r.h) and the [source file](memcrawl3r.c) in your project. The header file contains the declaration of the `memcrawl3r()` function and the structure for highlighting areas in the output.
   Note that memcrawl3r can be used directly in any common C project, so there is no need to use the original [makefile](makefile) or any special gcc flags.

 - as a shared library  
   If you compile memcrawl3r as a shared library (for example, with the original makefile and the command "make sharedlib"), you can link your projects against it.

 - as a preloaded shared library  
   Since both compiling directly and using shared library requires recompiling of the project when you want to add or remove memcrawl3r's functionality, a special makefile target was made - `make sharedlib-empty`. This target creates a library with an empty `memcrawl3r()` function, that does nothing (additionally you can place some "unsafe" code inside - see [stages of operation](#stages-of-operation) below). So if you compile against this shared library and run your program, it runs with the empty `memcrawl3r()` function, and whenever you need the memcrawl3r's output, you just preload a full shared library, which replaces the empty `memcrawl3r()` function with the original. For example of preloading libraries, see the target `preload` in the makefile. Also note that, when using the ASan, ASan's library must be preloaded first, otherwise your program won't run.

## Functionality
Memcrawl3r reads the memory only in blocks of inline assembly marked as volatile, to prevent any gcc's optimizer-based memory protection to wrap the memory reading instructions with the protective code. Additionally, it places labels around the instruction, so the address of the memory-accessing instruction and the address of the next instruction will be known for the signal handler. Then it captures all SIGSEGV signals with the custom signal handler, which is able to recover an execution directly after the instruction that caused the bad memory access, without causing a termination of the program or an infinite loop.

More advanced memory protection tools (like ASan, for example) do not allow to create custom signal handlers, so memcrawl3r has built-in functionalities to override these protections. The method used for overriding is chosen with the macro `MEMCRAWL3R_SIGNAL_OVERRIDE`. The default value is 2, a different value can be specified in [memcrawl3r.c](memcrawl3r.c) at line 15, or as an argument `-DMEMCRAWL3R_SIGNAL_OVERRIDE=` to the gcc. Meaning of the values:  
0 - do not use any overriding method, use the standard call to `sigaction()`  
1 - this is useful only with ASan - memcrawl3r specifies ASan's option `allow_user_segv_handler=1` and hopes that ASan allows custom signal handlers (note that this method doesn't always  work reliably, because it relies on the ASan's implementation)  
2 - this method is useful against everything that uses LD_PRELOAD or any similar method to replace the system functions - memcrawl3r gets handler of real `sigaction()` directly from the library `libc.so.6`

In all cases, memcrawl3r restores the previous signal handler before return.

### Function parameters
`size_t memcrawl3r(void *addr, size_t max_bytes_before, size_t max_bytes_after, int hl_len, memcrawl3r_highlight *mcr_hl, FILE *outfile)`

`addr` - the address from which memcrawl3r starts to read memory
`max_bytes_before` - the maximum value of how much bytes have to be printed before the provided address
`max_bytes_after` - the maximum value of how much bytes have to be printed after the provided address
`hl_len` - the number of members in an array of the highlight structures
`mcr_hl` - the pointer to an array of the highlighted structures, each structure contains an address, and the number of bytes that have to be highlighted from it
`outfile` - the file (or the stream) to which memcrawl3r prints all of the normal output, error and debug lines are still printed to the standard error output

### Highlighting of output
Memcrawl3r provides a simple color highlighting of the memory regions in the output, for a better visualization.
Highlighting works by the following rules:
 - if the `outfile` argument is different than `stdout` or `stderr`, all color output is disabled
 - if the order of the highlighted area in the array `mcr_hl` is odd, then the output color is red
 - if the order of the highlighted area in the array `mcr_hl` is even, then the output color is blue
 - if the currrently printed byte lies in more than one of highlight areas, then the output color is magenta
 - if the currrently printed byte lies directly at the address `addr`, then its color is inverted

## Stages of operation
Memcrawl3r's functionality is divided into 5 "stages":
1. Checking if the address `addr` points at a readable byte of the memory - If read causes the SIGSEGV, memcrawl3r recovers from the signal handler, prints an error message, and returns zero. Otherwise, if the address is readable, it proceeds to the stage 2.
2. Checking how many bytes ahead of the given address are readable - Memcrawl3r reads bytes from addresses lower than the provided address, until it finds the first byte which causes the SIGSEGV, to figure the memory segment's lower boundary.
3. Printing the requested number of bytes - If there are more readable bytes before `addr` than the value of the argument `max_bytes_before`, printing starts from `addr - max_bytes_before`. If `max_bytes_before` is zero, all readable bytes are printed. Printing continues until the program receives SIGSEGV at the upper boundary of the segment, or until all the bytes up to `addr + max_bytes_after` are printed. If all the requested bytes are printed, and a SIGSEGV was not received, memcrawl3r proceeds to the stage 4, otherwise it returns (or jumps to the stage 5, if it is being run as the preloaded library). The return value of the `memcrawl3r()` function is the number of successfully printed bytes.
4. Checking how many bytes after the given address are readable - Memcrawl3r reads the bytes from the last printed address, until it finds the first byte that causes the SIGSEGV to figure out the memory segment's upper boundary. After that, it returns, or, if it is being run as the preloaded shared library, continues to the stage 5.
5. This stage is available only when running memcrawl3r as the preloaded library - In this stage memcrawl3r calls the empty `memcrawl3r()` function which was replaced by the full `memcrawl3r()` function. The custom signal handler still catches any SIGSEGV, so any code inside will not crash the program, but since the address and the length of an instruction that caused the SIGSEGV is unknown, it cannot successfully recover the execution, so it ends the program by calling `exit(42)`. In this case, you can still specify cleanup routines with `atexit()`.

## Examples
Basic example of the memcrawl3r's functionality was built directly into the shared library. When you compile memcrawl3r with `make sharedlib-runnable`, a fully functional shared library is produced, but also the `main()` function is present inside, so it can be run as an standalone program simply by running `./libmemcrawl3r_runnable.so`. If you do this, the following output is produced, and you can see how it is possible to use memcrawl3r to track commandline arguments.

![screenshot1](screenshot1.png)

Another example is in file [main.c](main.c), where you can see the allocation of an 2D array which contains 3 rows of 5 cells of integers, where the first 3x8 bytes are occupied by the row pointers. This was compiled and run with `make run-static`.

![screenshot2](screenshot2.png)

## Some compilation notes
### Makefile in this project is only for gcc-7
Gcc-7 was used because it is default on my system, and I use a lot of compiler flags, some of which are specific to this gcc version. If you would like to compile with the original makefile, but with a different version of gcc, simply change the desired gcc version at line 3 in the makefile, and remove all of the flags causing errors.
### Memcrawl3r can be compiled as 32-bit and 64-bit
Although the first concepts of memcrawl3r were written only for 32-bit usage, I wanted to make it compatible with 64-bit mode, so it could be used in 64-bit applications. This needed the code to be compatible with both modes. For example, the pointers in 64-bit mode are of a different size. I solved it by using `PRIxPTR` macros inside the printf format specifiers and explicit type conversions wherever needed. But most importantly, 64-bit programs use [RIP-relative addressing](https://en.wikipedia.org/wiki/Addressing_mode#PC-relative) mode, and a different instruction pointer register (used in the structure received by the signal handler), so I solved it by using different macros that modify the source code.
### Debug mode is not only for debug
Debug mode causes memcrawl3r to print a lot of additional information, which can be useful. Debug mode can be enabled by uncommenting line 12 in [memcrawl3r.c](memcrawl3r.c), or using the compiler flag `-DMEMCRAWL3R_DEBUG` when compiling. Here is the screenshot of the second example, when compiled in debug mode:

![screenshot3](screenshot3.png)

## Author
Strojar42

## License
Memcrawl3r is released under the X11 license. See [LICENSE](LICENSE) for more details.
